from openerp import models, fields

class Flock(models.Model):
    _name = 'farm.flock'


    farm_code = fields.Many2one('res.partner', string='Farm Code',
                                ondelete='set null')
    unit_code = fields.Char('Unit Code')
    shed_code = fields.Many2many('res.partner', string='Shed Code')
    breed = fields.Many2many('res.partner', string='Breed')

    #Todo computed field flock_code will be needed unit_code+shed_code+farm_code

    start_date = fields.Date('Start Date')
    female_opening = fields.Integer('Female Opening')
    male_opening = fields.Integer('Male Opening')


class FlockTransfer(models.Model):
    _name = 'farm.transfer'

    transfer_date = fields.Date('Transfer Date')

    #Shed should be selections
    from_shed = fields.Char('From Shed')
    to_shed = fields.Char('To Shed')
    flock_code = fields.Selection(string="Flock Code",
                                  selection=[('23-21-GAA', '21'), ('23-21-GAA', '22'), ],
                                  required=False, )

    #Todo check if the number of birds available are less than number of birds transfered
    #using @api.constrains

    female_birds = fields.Integer('Female Birds')
    male_birds = fields.Integer('Male Birds')

class ResPartner(models.Model):
    _inherit = 'res.partner'
    flock_id = fields.One2many('farm.flock','farm_code',
                        string='Flock ID')
    shed_id = fields.Many2many('farm.flock','Sheds')
    breeds = fields.Many2many('farm.flock','Breeds')
