from openerp import models, fields

class Vaccine(models.Model):
    _name = 'farm.vaccine'

    #Todo should be a selection
    breed = fields.Char('Breed')

    age = fields.Integer('Age(weeks)')
    vaccine = fields.Char('Vaccine')

