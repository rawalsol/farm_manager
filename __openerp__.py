# -*- coding: utf-8 -*-
{
    'name': "Farm Manager",

    'summary': """
        Poultary Farm Management Application
        """,

    'description': """
        Empowering the Modern Poultary Farms to use Enterprise level
        Management Information Systems
    """,

    'author': "Shaheryar Malik",
    'website': "http://www.shaheryarmalik.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.10',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'application':True,
}